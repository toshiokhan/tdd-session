package org.epam.tdd;


import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestMultiplication {


    @Test
    void testMultiplication() throws Exception {
        Money five = Money.dollar(5);
        assertEquals(Money.dollar(10), five.multiply(2));
        assertEquals(Money.dollar(15), five.multiply(3));
    }

    @Test
    void testDollarEquality() throws Exception {

        assertTrue(new Dollar(5).equals(new Dollar(5)));
        assertFalse(new Dollar(6).equals(new Dollar(5)));
        assertFalse(new Franc(5).equals(new Dollar(5)));
    }

    @Test
    void testFrancMultiplication() throws Exception {
        Money five = Money.franc(5);
        Money product = five.multiply(2);
        assertEquals(10, product.amount);
    }


}
