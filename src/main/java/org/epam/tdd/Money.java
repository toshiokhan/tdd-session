package org.epam.tdd;

public  class Money {

    protected int amount ;

    public Money(int amount) {
        this.amount = amount;
    }

    public static Dollar dollar(int amount) {
       return new Dollar(amount);
    }

    public static Money franc(int i) {
        return new Franc(i);
    }


    public  Money multiply(int factor) {
        return new Money(amount * factor);

    }

    @Override
    public boolean equals(Object that){
        Money money = (Money)that;
        return this.amount == money.amount && getClass().equals(money.getClass());
    }

    @Override
    public String toString() {
        return "Money{" +
                "amount=" + amount +
                '}';
    }
}
